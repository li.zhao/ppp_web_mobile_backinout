require.config({
	map: {
		'*': {
			'css': 'lib/require-css/css'
		}
	},
	paths: {
		'zepto': 'lib/zepto.min',
		'zeptoex': 'lib/zeptoExtend',
		'vue': 'lib/vue/vue',
		'vueRoute': 'lib/vue/vue-router',		
		//'calendar': 'lib/calendar/mobiscroll.custom-3.0.0-beta.min',
		'layer': 'lib/layer_mobile/layer',		
		'base': 'base',
		//'dropgroup': 'lib/dropdownList/dropgroup',
		'index': 'index',		
		'routes': 'route',
		'txt': 'lib/text/text',
		//'dtree': 'lib/dtree/dtree',
		//'iscroll': 'lib/iscroll-probe',	
	},
	shim: {
		'zepto': {
			exports: '$'
		},
		//"dtree": ['css!./lib/dtree/dtree'],
		//'calendar': ['zepto', 'zeptoex', 'css!calendar'],
		'layer': ['zepto', 'css!./lib/layer_mobile/need/layer'],		
		//'dropgroup': ['css!dropgroup'],
		//'base': ['zepto', 'layer', 'dropgroup', 'vue', 'calendar', "dtree"],
		'base': ['layer', 'vue'],
		'vueRoute': ['vue'],
		//'iscroll':['zepto']
	}
});

require(['vue', 'vueRoute', 'routes',], function(v, v_route,routes) {
	v.use(v_route);

	//配置路由
	var router = new v_route({
		routes: routes
	});

	//配置vue参数；
	var startvue = {
		el: "#app",
		router: router,
		data: {
			navItem: [{
					name: '首页',
					path: '/',
					isShow: true
			}],
			isSet: true,
			isios: baseobj.browser("ios")
		},
		mounted: function() {
			
		    
		}
	};
	 
	new v(startvue);

});
define(['index'], function(IndexHTML) {
	var routes = [{
			name: 'index',
			path: '/',
			component: IndexHTML
	}]
	return routes;
});
define(['vue','txt!../model/Index.html','txt!../model/m_explain.html','txt!../model/disclaimer.html','base'], function(v,IndexHtml,m_explain,disclaimer) {

	var Index = v.extend({

		template: IndexHtml,

		data: function() {
			base = {
				keyword:'',	
				list:[],
				isCountryShow:false,
				countryName:'全国',
				countryList:[],
				isIndustryShow:false,
				industryName:'行业',
				industryList:[],
				isScreeningShow:false,				
				loadingStep:0,//加载状态 0默认，1继续加载下一页
				noMore:0,//列表加载完成显示
				page:1,
				province_code:'',
				industry_id:'',
				cpppc_is_out:1,
				kuName:'退库',
				kuList:[{
					name:'退库',
					code:'1',
					selected:true
				},{
					name:'入库',
					code:'0'
				}],
				isListShow:false,
				currentDate:baseobj.timer.dateformat(new Date(),'yyyy年MM月dd日'),
			};
			return base;
		},
		mounted: function() {
			var _this = this;
			this.getList();
			this.getScreenIng();						
		},
		methods: {	
			pageWizard:function(page){
				if(page<0){
					return;
				}
				base.page = page;
				this.getList();
				$(window).scrollTop(0);
			},
			getScreenIng:function(){				
				baseobj.getJsonData('m/get_enums',{},function(result){					
					if(result && result.data){	
						
						base.countryList.push({
							name:'不限',
							code:''
						});
						result.data.province.forEach(function(item){
							base.countryList.push(item);
						});
						base.industryList.push({
							name:'不限',
							id:''
						});
						result.data.industry.forEach(function(item){
							base.industryList.push(item);
						});
					}
				});
			},
		
			getList:function(){
				var params = {
					"page": base.page,
					"search_text":base.keyword,
					"province_code": base.province_code,
					"industry_id": base.industry_id,
					"cpppc_is_out":base.cpppc_is_out
				}
				let obj = {};
				for(var attr in params){
					if(params[attr]){
						obj[attr] = params[attr];
					}
				}
				
				baseobj.getJsonData('m/get_m_list',obj,function(result){
				
					if(result && result.data){	
						
						
						if(result.data.projects.length==0){
							base.isListShow = true;
						}else{
							result.data.projects.forEach(function(item){
								item.show=false;
							});
							base.isListShow = false;
						}
						base.list = result.data;
					}else{
						baseobj.layerobj.alertmsg("获取失败");
						base.isListShow = true;
					}
				});
				
			},
			closeScreening:function(event){	
				var _this = this;
				var _attr = event.target.attributes;
				if( _attr && _attr[0] && _attr[0]['nodeName'] && _attr[0]['value']=='ban'){}else{
					_this.toScreenings(0);
				}
			},
			toScreenings:function(index){				
				switch (index){
					case 1:
						base.isIndustryShow = false;
						base.isScreeningShow = false;
						base.isCountryShow = !base.isCountryShow;
						break;
					case 2:
						base.isCountryShow = false;
						base.isScreeningShow = false;
						base.isIndustryShow = !base.isIndustryShow;
						break;
					case 3:
						base.isCountryShow = false;
						base.isIndustryShow = false;
						base.isScreeningShow = !base.isScreeningShow;
						break;	
					default:
						base.isCountryShow = false;
						base.isIndustryShow = false;
						base.isScreeningShow = false;
						break;
				}
			},
			toChooseCountry:function(item){
				var _this = this;
				base.countryList.forEach(function(opt){
					opt.selected = false;
				});
				
				if(item.code){
					base.countryName = item.name;
					item.selected = true;
				}else{
					base.countryName = '全国';
				}
				base.isCountryShow = false;
				base.list = [];
				base.page = 1;
				base.province_code = item.code;
				this.getList();
				
			},
			toChooseIndustry:function(item){
				var _this = this;
				base.industryList.forEach(function(opt){
					opt.selected = false;
				});
				
				if(item.id){
					base.industryName = item.name;
					item.selected = true;
				}else{
					base.industryName = '行业';
				}
				base.isIndustryShow = false;
				base.list = [];
				base.page = 1;
				base.industry_id = item.id;
				this.getList();
			},
			toChooseKu:function(item){
				var _this = this;
				base.kuList.forEach(function(opt){
					opt.selected = false;
				});
				
				if(item.code){
					base.kuName = item.name;
					item.selected = true;
				}else{
					base.kuName = '退库';
				}
				base.isScreeningShow = false;
				base.list = [];
				base.page = 1;
				base.cpppc_is_out = item.code;
				this.getList();
			},
			toSearch:function(){
				base.list = [];
				base.page = 1;
				this.getList();
			},
			toShowItem:function(item,index){				
				item.show = !item.show;				
			},
			explain:function(){
				
	           layer.open({
					type: 1,
					content: m_explain,
					title:['数据说明','background-color:#eee;padding:0px'],
	                area: ['90%', '50%'],
	                className:'list-layer',
	                shadeClose: true,
	                offset:'20%',
	                style:'width:90%;margin:0 auto;border-radius:5px;'
				});
				
			},
			disclaimer:function(){
				layer.open({
					type: 1,
					content: disclaimer,
					title:['免责声明','background-color:#eee;padding:0px'],
	                area: ['90%', '50%'],
	                className:'list-layer',
	                shadeClose: true,
	                offset:'20%',
	                style:'width:90%;margin:0 auto;border-radius:5px;'
				});
			}
		}
	});
	
	
	return Index;
});